export const SET_CARS = 'SET_CARS';
export const SET_LOADING = 'SET_LOADING';
export const SET_PAGINATE_LOADING = 'SET_PAGINATE_LOADING';
export const SET_REFRESH_LOADING = 'SET_REFRESH';
export const SET_ERROR = 'SET_ERROR';
export const SET_PAGE = 'SET_PAGE';
export const SET_MODAL_VISIBLE = 'SET_MODAL_VISIBLE';
export const SET_MODAL_DATA = 'SET_MODAL_DATA';
