import React, {useReducer} from 'react';
import {AppContext} from './AppContext';
import AppReducer, {initialState} from './AppReducer';
import {
  SET_CARS,
  SET_ERROR,
  SET_LOADING,
  SET_MODAL_DATA,
  SET_MODAL_VISIBLE,
  SET_PAGE,
  SET_PAGINATE_LOADING,
  SET_REFRESH_LOADING,
} from './action-types';
import Config from 'react-native-config';
import {imagesArray, paginating} from '../helpers';
import {Car} from './types';

export const AppState: React.FC = ({children}) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  const setLoading = (loading: boolean) => {
    dispatch({
      type: SET_LOADING,
      loading,
    });
  };

  const setPaginateLoading = (paginate: boolean) => {
    dispatch({
      type: SET_PAGINATE_LOADING,
      paginate,
    });
  };

  const setRefreshLoading = (refresh: boolean) => {
    dispatch({
      type: SET_REFRESH_LOADING,
      refresh,
    });
  };

  const setError = <T extends unknown>(error: T) => {
    dispatch({
      type: SET_ERROR,
      error,
    });
  };

  const setPageIncrement = () => {
    setPaginateLoading(true);
    dispatch({
      type: SET_PAGE,
      page: state.page + 1,
    });
  };

  const setPageDefault = () => {
    setRefreshLoading(true);
    dispatch({
      type: SET_PAGE,
      page: 1,
    });
  };

  const setModalVisible = (value: boolean) => {
    dispatch({
      type: SET_MODAL_VISIBLE,
      modalIsVisible: value,
    });
  };

  const setModalData = (car: Car) => {
    dispatch({
      type: SET_MODAL_DATA,
      modalData: car,
    });
  };

  //Получение данных
  const fetchCars = async (): Promise<Car[]> => {
    if (!state.isPaginate && !state.isRefresh) {
      setLoading(true);
    }
    const url = Config.BASE_API_URL;
    return fetch(`${url}/cars`)
      .then(response => response.json())
      .then(response => {
        let tempResponse = response.map((car: Car) => {
          const id = Number(car.id.toString()[car.id.toString().length - 1]);
          car.img_url = imagesArray[id];
          return car;
        });
        dispatch({
          type: SET_CARS,
          carsList: paginating<Car[]>(state.page, 10, tempResponse),
        });
        setLoading(false);
        setPaginateLoading(false);
        setRefreshLoading(false);
        return tempResponse;
      })
      .catch(e => {
        setError(e);
        setLoading(false);
        setPaginateLoading(false);
        setRefreshLoading(false);
        throw e;
      });
  };

  return (
    <AppContext.Provider
      value={{
        carsList: state.carsList,
        isRefresh: state.isRefresh,
        isLoading: state.isLoading,
        isPaginate: state.isPaginate,
        error: state.error,
        page: state.page,
        modalIsVisible: state.modalIsVisible,
        modalData: state.modalData,
        fetchCars,
        setPageIncrement,
        setPageDefault,
        setModalVisible,
        setModalData,
      }}>
      {children}
    </AppContext.Provider>
  );
};
