import {ImageSourcePropType} from 'react-native';

export type Car = {
  year: number;
  id: number;
  horsepower: number;
  make: string;
  model: string;
  price: number;
  img_url: ImageSourcePropType;
};

export type State = {
  carsList: Car[];
  error: string | null;
  isRefresh: boolean;
  isLoading: boolean;
  isPaginate: boolean;
  page: number;
  modalIsVisible: boolean;
  modalData: Partial<Car>;
};

export type ProviderContext = State & {
  fetchCars: () => void;
  setPageIncrement: () => void;
  setPageDefault: () => void;
  setModalVisible: (value: boolean) => void;
  setModalData: (car: Car) => void;
};
