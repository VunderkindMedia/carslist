import {createContext} from 'react';
import {ProviderContext} from '@src/store/types';
import {initialState} from '@src/store/AppReducer';

const defaultContext = {
  ...initialState,
  fetchCars: () => {},
};

export const AppContext = createContext<ProviderContext>(defaultContext);
