import {
  SET_CARS,
  SET_ERROR,
  SET_LOADING,
  SET_MODAL_DATA,
  SET_MODAL_VISIBLE,
  SET_PAGE,
  SET_PAGINATE_LOADING,
  SET_REFRESH_LOADING,
} from './action-types';
import {State} from './types';

export const initialState: State = {
  carsList: [],
  isRefresh: false,
  isLoading: false,
  isPaginate: false,
  error: null,
  modalData: {},
  modalIsVisible: false,
  page: 1,
};

export default function AppReducer(state: State, action: any) {
  switch (action.type) {
    case SET_CARS:
      return {...state, carsList: action.carsList};
    case SET_LOADING:
      return {...state, isLoading: action.loading};
    case SET_PAGINATE_LOADING:
      return {...state, isPaginate: action.paginate};
    case SET_REFRESH_LOADING:
      return {...state, isRefresh: action.refresh};
    case SET_ERROR:
      return {...state, error: action.error};
    case SET_PAGE:
      return {...state, page: action.page};
    case SET_MODAL_VISIBLE:
      return {...state, modalIsVisible: action.modalIsVisible};
    case SET_MODAL_DATA:
      return {...state, modalData: action.modalData};
    default:
      return state;
  }
}
