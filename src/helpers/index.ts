export const paginating = <C>(
  page: number,
  page_size: number,
  data: C[],
): C[] => {
  return data.slice(0, page * page_size);
};

export const imagesArray = [
  require('@src/assets/images/auto_1.png'),
  require('@src/assets/images/auto_2.png'),
  require('@src/assets/images/auto_3.png'),
  require('@src/assets/images/auto_4.png'),
  require('@src/assets/images/auto_5.png'),
  require('@src/assets/images/auto_6.png'),
  require('@src/assets/images/auto_1.png'),
  require('@src/assets/images/auto_3.png'),
  require('@src/assets/images/auto_5.png'),
  require('@src/assets/images/auto_2.png'),
];
