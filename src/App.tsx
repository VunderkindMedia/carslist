import React from 'react';
import Navigation from './modules/navigations';
import {AppState} from '@src/store/AppState';

const App = () => {
  return (
    <AppState>
      <Navigation />
    </AppState>
  );
};

export default App;
