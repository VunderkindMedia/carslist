import React from 'react';
import {ActivityIndicator} from 'react-native';

type Props = {
  color?: string;
  size?: number | 'small' | 'large' | undefined;
};

export const UDLoader = (props: Props) => {
  const {color, size} = props;
  return <ActivityIndicator size={size || 'small'} color={color || 'red'} />;
};
