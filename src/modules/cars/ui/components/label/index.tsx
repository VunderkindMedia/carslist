import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

type Props = {
  color?: string;
  borderRadius?: number;
  title: string;
  titleFontSize?: number;
  titleColor?: string;
  containerStyle?: {};
};

const UDLabel = (props: Props) => {
  const {
    color,
    borderRadius,
    title,
    titleFontSize,
    titleColor,
    containerStyle,
  } = props;

  return (
    <View
      style={[
        styles.container,
        containerStyle,
        {backgroundColor: color || '#F16764', borderRadius: borderRadius || 25},
      ]}>
      <Text
        style={[
          styles.title,
          {fontSize: titleFontSize || 16, color: titleColor || '#fff'},
        ]}>
        {title}
      </Text>
    </View>
  );
};

export default UDLabel;

const styles = StyleSheet.create({
  container: {
    height: 34,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontWeight: 'bold',
  },
});
