import React, {useCallback} from 'react';
import {TouchableOpacity, View, Text, Image, StyleSheet} from 'react-native';
import {Car} from '@src/store/types';
import UDLabel from '@src/modules/cars/ui/components/label';

type Props = {
  car: Car;
  onPressed: (car: Car) => void;
};

const CarsComponentsCarsListItem = (props: Props) => {
  const {car, onPressed} = props;
  const clickHandler = useCallback(() => onPressed(car), [car, onPressed]);

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.container}
      onPress={clickHandler}>
      <Image source={car.img_url} style={styles.image} />
      <View style={styles.contentWrapper}>
        <Text style={styles.title}>{car.year}</Text>
        <Text style={styles.modelTitle}>
          {car.make} {car.model}
        </Text>
        <View style={styles.footerWrapper}>
          <UDLabel
            title={`${car.price} $`}
            borderRadius={10}
            containerStyle={styles.labelContainerStyle}
          />
          <UDLabel
            title={`${car.horsepower} Hp`}
            containerStyle={styles.labelContainerStyle}
            borderRadius={10}
            color={'#FFEDED'}
            titleColor={'#F16764'}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CarsComponentsCarsListItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    marginVertical: 10,
    paddingBottom: 15,
    borderBottomColor: '#DADADA',
    borderBottomWidth: 0.7,
  },
  contentWrapper: {
    flex: 1,
  },
  image: {
    width: '100%',
    height: 100,
    resizeMode: 'cover',
  },
  title: {
    fontSize: 16,
    marginBottom: 5,
  },
  modelTitle: {
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 10,
  },
  footerWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  labelContainerStyle: {marginRight: 5, flex: 1},
});
