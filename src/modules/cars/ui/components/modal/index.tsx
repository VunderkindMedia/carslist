import React from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {Car} from '@src/store/types';
import Modal from 'react-native-modal';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons';
import UDLabel from '@src/modules/cars/ui/components/label';

type Props = {
  isVisible: boolean;
  car: Partial<Car>;
  onPressClose: () => void;
};

const UDModal = (props: Props) => {
  const {isVisible, car, onPressClose} = props;
  return (
    <Modal isVisible={isVisible}>
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.closeButtonWrapper}
          onPress={onPressClose}>
          <FontAwesomeIcon size={24} icon={faTimes} />
        </TouchableOpacity>
        <View>
          <Image
            source={car.img_url || require('@src/assets/images/auto_1.png')}
          />
          <View style={styles.contentWrapper}>
            <View style={styles.columnWrapper}>
              <Text style={styles.textStyle}>Year: {car.year}</Text>
              <Text style={styles.textStyle}>Consumer: {car.make}</Text>
            </View>
            <View style={styles.columnWrapper}>
              <Text style={styles.textStyle}>Model: {car.model}</Text>
              <Text style={styles.textStyle}>
                Hourse Power: {car.horsepower}
              </Text>
            </View>
          </View>
          <Text style={styles.textStyle} />
        </View>
        <UDLabel
          title={`Price: ${car.price} $`}
          containerStyle={styles.labelContainerStyle}
          borderRadius={10}
          titleFontSize={20}
        />
      </View>
    </Modal>
  );
};

export default UDModal;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    height: 320,
    width: '100%',
    borderRadius: 25,
    padding: 25,
  },
  textStyle: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  contentWrapper: {
    flexDirection: 'row',
    paddingVertical: 20,
  },
  columnWrapper: {
    alignItems: 'center',
    flex: 1,
  },
  closeButtonWrapper: {
    padding: 10,
    position: 'absolute',
    right: 15,
    top: 15,
  },
  labelContainerStyle: {height: 50},
});
