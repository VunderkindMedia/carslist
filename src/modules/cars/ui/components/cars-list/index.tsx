import React from 'react';
import {FlatList, StyleSheet} from 'react-native';
import {Car} from '@src/store/types';
import CarsComponentsCarsListItem from '@src/modules/cars/ui/components/cars-list-item';
import {UDLoader} from '@src/modules/ud-ui/components/loader';

type Props = {
  cars: Car[];
  isRefresh: boolean;
  onRefresh: () => void;
  onPaginate: () => void;
  onClickItem: (car: Car) => void;
  isPaginate: boolean;
};

const CarsComponentsCarsList = (props: Props) => {
  const {cars, isRefresh, onRefresh, onPaginate, isPaginate, onClickItem} =
    props;

  return (
    <FlatList
      contentContainerStyle={styles.listContainer}
      data={cars}
      refreshing={isRefresh}
      onRefresh={onRefresh}
      numColumns={2}
      renderItem={({item}) => (
        <CarsComponentsCarsListItem car={item} onPressed={onClickItem} />
      )}
      onEndReached={onPaginate}
      onEndReachedThreshold={0.1}
      initialNumToRender={10}
      keyExtractor={item => item.id.toString()}
      ListFooterComponent={isPaginate ? <UDLoader /> : null}
    />
  );
};

export default CarsComponentsCarsList;

const styles = StyleSheet.create({
  listContainer: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
});
