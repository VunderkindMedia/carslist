import React, {useContext, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {AppContext} from '@src/store/AppContext';
import {UDLoader} from '@src/modules/ud-ui/components/loader';
import CarsComponentsCarsList from '@src/modules/cars/ui/components/cars-list';
import UDModal from '@src/modules/cars/ui/components/modal';
import {Car} from '@src/store/types';

const CarsScreensList = () => {
  const {
    carsList,
    fetchCars,
    page,
    setPageIncrement,
    setPageDefault,
    isLoading,
    isRefresh,
    isPaginate,
    modalIsVisible,
    setModalVisible,
    setModalData,
    modalData,
  } = useContext(AppContext);
  useEffect(() => {
    fetchCars();
  }, [page]);

  const itemClickHandler = (car: Car) => {
    setModalVisible(true);
    setModalData(car);
  };

  const modalCloseHandler = () => {
    setModalVisible(false);
  };

  return isLoading ? (
    <View style={styles.loaderWrapper}>
      <UDLoader color={'red'} size={'large'} />
    </View>
  ) : (
    <View>
      <CarsComponentsCarsList
        isRefresh={isRefresh}
        cars={carsList}
        onPaginate={() => setPageIncrement()}
        onRefresh={() => setPageDefault()}
        isPaginate={isPaginate}
        onClickItem={(car: Car) => itemClickHandler(car)}
      />
      <UDModal
        isVisible={modalIsVisible}
        car={modalData}
        onPressClose={modalCloseHandler}
      />
    </View>
  );
};
export default CarsScreensList;

const styles = StyleSheet.create({
  loaderWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
