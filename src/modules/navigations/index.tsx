import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {ScreenNames} from './screen-names';
import CarsScreensList from '../cars/ui/screens/list';

const Stack = createStackNavigator();

export default function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={ScreenNames.CarsList}
        screenOptions={{cardStyle: {backgroundColor: '#fff'}}}>
        <Stack.Screen
          name={ScreenNames.CarsList}
          component={CarsScreensList}
          options={{
            title: 'Cars',
            headerStyle: {
              backgroundColor: '#251F37',
            },
            headerTitleStyle: {
              color: 'white',
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
