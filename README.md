##Тестовое задание

- API Endpoint: https://private-anon-55c6042590-carsapi1.apiary-mock.com/cars
- Изображения добавил локально, так как API частично не имеет изображений.

Приложение выполнено на базе React Native CLI без применения сторонних библиотек, кроме указанных ниже:
- FontAwesome Icons
- React Navigation

Для запуска приложения необходимо развернуть проект из данного репозитория и установить все зависимости - `npm install` (необходимо убедиться, установлен ли `node js` и `cocoapods`)

###Android
1. Добавьте путь к android SDK в файл `android/local.properties`: `sdk.dir = /Users/#Ваш пользователь#/Library/Android/sdk` (mac os).
2. Запустите проект коммандой `npm run android`

###iOS
1. Перейдите в директорию `ios` (cd ios) и введите комманду `pod install`.
2. Запустите проект коммандой `npm run ios`
